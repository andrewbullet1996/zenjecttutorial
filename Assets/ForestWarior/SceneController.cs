using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class SceneController : ITickable, IInitializable
{
    [Inject] private PlayerHandler _playerHandler;
    private PlayerBase _player;

    public void Initialize()
    {
        _player = _playerHandler.Player;
    }

    public void Tick()
    {
        if (Input.GetKeyDown(KeyCode.D))
        {
            _player.IsMoving = true;
            _player.TurnCharacter(false);
            _player.SetAnimationActivity(true);
        }
        if (Input.GetKeyUp(KeyCode.D))
        {
            _player.IsMoving = false;
            _player.SetAnimationActivity(false);
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            _player.IsMoving = true;
            _player.TurnCharacter(true);
            _player.SetAnimationActivity(true);
        }

        if (Input.GetKeyUp(KeyCode.A))
        {
            _player.IsMoving = false;
            _player.SetAnimationActivity(false);
        }
    }
}
