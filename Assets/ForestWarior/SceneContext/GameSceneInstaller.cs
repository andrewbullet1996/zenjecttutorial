using UnityEngine;
using Zenject;

public class GameSceneInstaller : MonoInstaller
{
    public override void InstallBindings()
    {
        Container.Bind<GameCamera>().FromComponentInHierarchy(true).AsSingle();
        Container.Bind<PlayerHandler>().FromComponentInHierarchy(true).AsSingle();
        Container.Bind<GameSettings>().FromComponentInHierarchy(true).AsSingle();

        Container.BindInterfacesAndSelfTo<SceneController>().AsSingle();
    }
}