using UnityEngine;

[RequireComponent(typeof(CapsuleCollider2D))]
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(SpriteRenderer))]
public abstract class PlayerBase : MonoBehaviour
{
    [SerializeField] private Animator _animator;
    [SerializeField] private SpriteRenderer _spriteRenderer;
    private Transform Transform => transform;

    public float Speed;

    private string _runnningParameter = "Run";

    public bool IsMoving;

    private void Update()
    {
        if (IsMoving)
        {
            bool side = _spriteRenderer.flipX;

            if (!side)
            {
                Transform.Translate(Speed * Time.deltaTime, 0, 0);
            }
            else
            {
                Transform.Translate(-Speed * Time.deltaTime, 0, 0);
            }
        }
    }

    public void TurnCharacter(bool side)
    {
        if (side)
        {
            _spriteRenderer.flipX = true;
        }
        else
        {
            _spriteRenderer.flipX = false;
        }
    }
    public void SetAnimationActivity(bool activity)
    {
        _animator.SetBool(_runnningParameter, activity);
    }
}
