using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class PlayerHandler : MonoBehaviour
{
    [Inject] private GameSettings _settings;

    [SerializeField] private PlayerBase _playerPrefab;
    public PlayerBase Player { get; private set; }

    private Transform Transform => transform;

    private void Awake()
    {
        Player = Instantiate(_playerPrefab, Transform);
        Player.Speed = _settings.PlayerSpeed;
    }
}
