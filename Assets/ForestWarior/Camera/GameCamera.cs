using Cinemachine;

using UnityEngine;

using Zenject;

public class GameCamera : MonoBehaviour
{
    [Inject] private PlayerHandler _playerHandler;
    [SerializeField] private CinemachineVirtualCamera _virtualCamera;

    private void Start()
    {
        Transform playerPosition = _playerHandler.Player.transform;

        _virtualCamera.Follow = playerPosition.transform;
        _virtualCamera.LookAt = playerPosition.transform;
    }
}
